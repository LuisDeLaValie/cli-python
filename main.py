#!/usr/bin/env python

import click


@click.group()
def inicio():
    pass


@inicio.command()
def saludar_user():
    print('hola, nos encontramos en el webinar de CF')


@inicio.command()
def despide():
    print('adios culos')


@inicio.command()
@click.argument('username')
# @click.option('--password', '-p', help='contraseña', prompt='Contraseña', hide_input=True)
@click.option('--password', '-p', help='contraseña', prompt='Contraseña', hide_input=True,confirmation_prompt=True)
# @click.password_option()
@click.option('--email', '-e', default='TDTx@outlook.com', help='Correo de ti')
@click.option('--active', '-a', is_flag=True, default=True)
def create_user(username, password, email, active):
    print('Nombre del Usuario: ', username)
    print('Nombre del pass: ', password)
    print('Nombre del correo: ', email)
    print('Nombre del activo: ', active)


if __name__ == "__main__":
    inicio()
